*** Settings ***
Resource          Variable.robot
Library           Selenium2Library
Library           Process
Library           Collections

*** Variables ***

*** Test Cases ***
Test
    log    Test1
    Comment    Comment    Open Browser    http://www.lc.mahidol.ac.th/th/StudentsAlumni/Regulation.htm    ie
    Comment    Process.Run Process    Taskkill /IM IEDriverServer.exe /F    shell=True
    Comment    ${caps}=    Evaluate    sys.modules['selenium.webdriver'].DesiredCapabilities.INTERNETEXPLORER    sys,selenium.webdriver
    Comment    Set To Dictionary    ${caps}    ignoreProtectedModeSettings    ${True}
    Comment    Set To Dictionary    ${caps}    ie.forceCreateProcessApi    ${True}
    Comment    Set To Dictionary    ${caps}    ie.browserCommandLineSwitches    -private
    Comment    Set To Dictionary    ${caps}    e.ensureCleanSession    ${True}
    Comment    Create Webdriver    Ie    capabilities=${caps}
    Comment    Go To    http://www.lc.mahidol.ac.th/th/StudentsAlumni/Regulation.htm
    Comment    ${setwindowsize_status}    BuiltIn.Run Keyword And Return Status    Selenium2Library.Set Window Size    1366    768
    Comment    BuiltIn.Run Keyword If    ${True} != ${setwindowsize_status}    Selenium2Library.Maximize Browser Window
    Comment    sleep    5
    Comment    Click Middle Element    //a[@href='../../documents/Students-Alumni/2016-ProcedureConducting .pdf']
