*** Settings ***
Resource          Variable.robot
Library           Selenium2Library
Library           Process
Library           Collections

*** Variables ***

*** Test Cases ***
Test
    log    ${num1}
    log    ${num2}
    log    ${expect}
    ${result}    Evaluate    ${num1} + ${num2}
    Should Be Equal As Integers    ${expect}    ${result}
