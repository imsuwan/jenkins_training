*** Settings ***
Resource          Variable.robot

*** Test Cases ***
TC3
    log    ${num1}
    log    ${num2}
    log    ${expect}
    ${result}    Evaluate    ${num1}+${num2}
    Should Be Equal As Integers    ${expect}    ${result}
