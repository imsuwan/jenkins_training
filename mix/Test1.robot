*** Settings ***
Resource          Variable.robot
Library           Selenium2Library
Library           Process
Library           Collections

*** Variables ***

*** Test Cases ***
Test
    log    ${num1}
    log    ${num2}
    log    ${expect2}
    ${result}    Evaluate    ${num1}-${num2}
    Should Be Equal As Integers    ${expect2}    ${result}
    Comment    Set To Dictionary    ${caps}    ie.forceCreateProcessApi    ${True}
    Comment    Set To Dictionary    ${caps}    ie.browserCommandLineSwitches    -private
    Comment    Set To Dictionary    ${caps}    e.ensureCleanSession    ${True}
    Comment    Create Webdriver    Ie    capabilities=${caps}
    Comment    Go To    http://www.lc.mahidol.ac.th/th/StudentsAlumni/Regulation.htm
    Comment    ${setwindowsize_status}    BuiltIn.Run Keyword And Return Status    Selenium2Library.Set Window Size    1366    768
    Comment    BuiltIn.Run Keyword If    ${True} != ${setwindowsize_status}    Selenium2Library.Maximize Browser Window
    Comment    sleep    5
    Comment    Click Middle Element    //a[@href='../../documents/Students-Alumni/2016-ProcedureConducting .pdf']
