*** Settings ***
Resource          Variable.robot

*** Test Cases ***
Test
    log    ${num1}
    log    ${num2}
    log    ${expect3}
    ${result}    Evaluate    ${num1}*${num2}
    Should Be Equal As Integers    ${expect3}    ${result}
