*** Settings ***
Resource          Variable.robot

*** Test Cases ***
Test
    log    ${num1}
    log    ${num2}
    log    ${expect4}
    ${result}    Evaluate    ${num1}/${num2}
    Should Be Equal As Integers    ${expect4}    ${result}
