*** Settings ***
Resource          Variable.robot

*** Test Cases ***
Test
    log    ${num1}
    log    ${num2}
    log    ${num3}
    log    ${expect5}
    ${result}    Evaluate    ${num1}+${num2}+${num3}
    Should Be Equal As Integers    ${expect5}    ${result}
