*** Settings ***
Resource          Variable.robot

*** Test Cases ***
Test
    Log    ${num1}
    Log    ${num2}
    Log    ${expect5}
    ${result}    Evaluate    ${num1}+${num2}+${num3}
    Should Be Equal As Integers    ${expect5}    ${result}
