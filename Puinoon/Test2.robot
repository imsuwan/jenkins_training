*** Settings ***
Resource          Variable.robot

*** Test Cases ***
Test
    Log    ${num1}
    Log    ${num2}
    Log    ${expect2}
    ${result}    Evaluate    ${num1}-${num2}
    Should Be Equal As Integers    ${expect2}    ${result}
