*** Settings ***
Resource          Variable.robot
Library           Selenium2Library
Library           Process
Library           Collections

*** Variables ***

*** Test Cases ***
Test
    Log    ${num1}
    Log    ${num2}
    Log    ${expect}
    ${result}    Evaluate    ${num1}+${num2}
    Should Be Equal As Integers    ${expect}    ${result}
