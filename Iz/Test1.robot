*** Settings ***
Resource          Variable.txt

*** Variables ***

*** Test Cases ***
Test
    log    ${Num1}
    log    ${Num2}
    log    ${Expect}
    ${Result}    Evaluate    ${Num1} + ${Num2}
    Should Be Equal As Integers    ${Expect}    ${Result}
